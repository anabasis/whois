# whois

A whois client written in Go.

![GIF demonstrating use of whois](demo.gif)

Install by running `go install codeberg.org/anabasis/whois@latest`.
Simply run `whois` or `whois example.com` to use it.

---

This isn't meant to be a full-scale implementation of the WHOIS protocol but just a way to look at some basic information about domains and IP addresses.

This client is based on the [ICANN lookup](https://lookup.icann.org) service.
Similar to the ICANN service, this client will not work for all TLDs (it excludes .io, .gov, and a lot more).

`whois` is licensed under the MIT License.

## Acknowledgments
* [color](https://github.com/fatih/color) (&copy; 2013 by Fatih Arslan under the [MIT License](https://github.com/fatih/color/blob/main/LICENSE.md))
* [ASCII Table Writer](https://github.com/olekukonko/tablewriter) (&copy; 2014 by Oleku Konko under the [MIT License](https://github.com/olekukonko/tablewriter/blob/master/LICENSE.md))
* [Go language](https://go.dev) (&copy; 2009 by The Go Authors under the [BSD 3-Clause License](https://go.dev/LICENSE))
