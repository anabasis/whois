package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"time"

	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
	"golang.org/x/net/publicsuffix"
)

const USLayout = "Monday, January 2, 2006 at 15:04:05 MST"

type RdapBootstrap struct {
	Service [][][]string `json:"services"`
}

type tldRdap struct {
	Links []struct {
		Rel  string `json:"rel"`
		Href string `json:"href"`
	} `json:"links"`
	Entities []struct {
		Roles []string     `json:"roles"`
		VCard [][][]string `json:"vCardArray"`
	} `json:"entities"`
	Events []struct {
		Action string     `json:"eventAction"`
		Date   *time.Time `json:"eventDate"`
	} `json:"events"`
	NameServers []struct {
		LdhName string `json:"ldhName"`
	} `json:"nameservers"`
	Status []string `json:"status"`
}

type ipRdap struct {
	Type     string   `json:"type"`
	Name     string   `json:"name"`
	Status   []string `json:"status"`
	Entities []struct {
		Handle string       `json:"handle"`
		Roles  []string     `json:"roles"`
		VCard  [][][]string `json:"vCardArray"`
	} `json:"entities"`
	Events []struct {
		Action string     `json:"eventAction"`
		Date   *time.Time `json:"eventDate"`
	} `json:"events"`
}

var (
	bold     *color.Color = color.New(color.Bold)
	red      *color.Color = color.New(color.FgHiRed)
	boldcyan *color.Color = color.New(color.Bold, color.FgHiCyan)
)

func main() {
	var input string
	if len(os.Args[1:]) > 0 {
		input = os.Args[1]
	} else {
		bold.Printf("Enter domain or IP: ")
		fmt.Scan(&input)
		fmt.Println("")
	}

	ipv4Regex := regexp.MustCompile(`[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}`)
	// TODO: real domain regex
	domainRegex := regexp.MustCompile(`.*\..*`)

	switch {
	case ipv4Regex.MatchString(input):
		WhoisIpv4(input)
	case domainRegex.MatchString(input):
		WhoisDomain(input)
	default:
		red.Println("Error")
		os.Exit(1)
	}
}

func WhoisDomain(input string) {
	tld, icann := publicsuffix.PublicSuffix(input)
	if !icann {
		red.Println("The TLD for this domain is unsupported!")
		os.Exit(1)
	}

	domain, err := publicsuffix.EffectiveTLDPlusOne(input)
	if err != nil {
		log.Fatalln(err)
	}

	data := GetDomainRdapData().Service
	var rdapRegistrar string
	for _, list := range data {
		for _, tlds := range list[0] {
			if tld == tlds {
				rdapRegistrar = list[1][0]
				break
			}
		}
	}
	if rdapRegistrar == "" {
		red.Println("Error finding domain. The TLD may be unsupported!")
		os.Exit(1)
	}

	registryData, err := GetTldRdapData(domain, rdapRegistrar)

	if err != nil {
		red.Println(err)
		os.Exit(1)
	}

	var domainRegistrar string
	for _, link := range registryData.Links {
		if link.Rel == "related" {
			domainRegistrar = link.Href
		}
	}

	// TODO: try getting data from registrar and fallback to TLD operator's

	boldcyan.Printf("TLD RDAP URL: ")
	fmt.Printf("%sdomain/%s\n", rdapRegistrar, domain)
	boldcyan.Printf("Domain registrar RDAP URL: ")
	fmt.Printf("%s\n", domainRegistrar)

	boldcyan.Println("\nNameservers:")
	for _, nameserver := range registryData.NameServers {
		fmt.Println(nameserver.LdhName)
	}

	boldcyan.Println("\nEvents:")
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Event", "Date"})
	for _, v := range registryData.Events {
		table.Append([]string{v.Action, v.Date.Format(USLayout)})
	}
	table.SetBorders(tablewriter.Border{Left: false, Top: false, Right: false, Bottom: false})
	table.SetRowSeparator("─")
	table.SetCenterSeparator("┼")
	table.SetColumnSeparator("│")
	table.Render()

	boldcyan.Printf("\nStatus: ")
	fmt.Println(registryData.Status[0])

	boldcyan.Println("\nInformation:")
	for _, msg := range registryData.Entities {
		if msg.VCard != nil {
			var entity string
			if len(msg.VCard[1]) > 1 {
				entity = " - " + msg.VCard[1][1][3]
			}
			fmt.Println(msg.Roles[0] + entity)
		}
	}
}

func GetDomainRdapData() RdapBootstrap {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://data.iana.org/rdap/dns.json", nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	data := string(body)

	var info RdapBootstrap
	json.Unmarshal([]byte(data), &info)
	return info
}

func GetTldRdapData(domain string, url string) (tldRdap, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url+"domain/"+domain, nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return tldRdap{}, fmt.Errorf("domain not found")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	data := string(body)

	var info tldRdap
	json.Unmarshal([]byte(data), &info)
	return info, nil
}

func WhoisIpv4(input string) {
	reipv4range := regexp.MustCompile(`\..*`)
	ipv4range := reipv4range.ReplaceAllString(input, `.0.0.0/8`)

	data := GetIpv4RdapData().Service
	var rdapRegistrar string
	for _, list := range data {
		for _, iprange := range list[0] {
			if ipv4range == iprange {
				rdapRegistrar = list[1][0]
				break
			}
		}
	}
	ipData := GetIpv4Info(input, rdapRegistrar)

	boldcyan.Printf("IP RDAP URL: ")
	fmt.Printf("%sip/%s\n", rdapRegistrar, input)

	boldcyan.Println("\nEvents:")
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Event", "Date"})
	for _, v := range ipData.Events {
		table.Append([]string{v.Action, v.Date.Format(USLayout)})
	}
	table.SetBorders(tablewriter.Border{Left: false, Top: false, Right: false, Bottom: false})
	table.SetRowSeparator("─")
	table.SetCenterSeparator("┼")
	table.SetColumnSeparator("│")
	table.Render()

	boldcyan.Println("\nStatus:")
	for _, msg := range ipData.Status {
		fmt.Println(msg)
	}

	boldcyan.Println("\nInformation:")
	for _, msg := range ipData.Entities {
		if msg.VCard != nil {
			fmt.Printf("%s - %s (%s)\n", msg.Roles[0], msg.VCard[1][1][3], msg.Handle)
		}
	}
}

func GetIpv4RdapData() RdapBootstrap {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://data.iana.org/rdap/ipv4.json", nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	data := string(body)

	var info RdapBootstrap
	json.Unmarshal([]byte(data), &info)
	return info
}

func GetIpv4Info(ip string, url string) ipRdap {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url+"ip/"+ip, nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0")

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	data := string(body)

	var info ipRdap
	json.Unmarshal([]byte(data), &info)
	return info
}
