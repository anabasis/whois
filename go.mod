module codeberg.org/anabasis/whois

go 1.17

require (
	github.com/fatih/color v1.15.0
	github.com/olekukonko/tablewriter v0.0.5
	golang.org/x/net v0.10.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
